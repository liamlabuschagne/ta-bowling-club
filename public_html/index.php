<?php
$page = str_replace("/", "", $_SERVER["REQUEST_URI"]);
if ($page == "") {
    $page = "index";
}

$pages = [
    "index" => ["Te Awamutu Bowling Club", "Welcome to the Te Awamutu Bowling Club website!"],
    "theclub" => ["The Club", "Find out all about the Te Awamutu Bowling Club"],
    "contact" => ["Contact", "Get in touch with the Te Awamutu Bowling Club"],
    "events" => ["Events", "See all the great events we have on offer at the Te Awamutu Bowling Club"],
    "programme" => ["Results", "Here you can download the the club programme."],
    "results" => ["Results", "Here you can download the latest results for competitions at the Te Awamutu Bowling Club"],
];

if ($pages[$page]) {
    $title = $pages[$page][0];
    $description = $pages[$page][1];
} else {
    http_response_code(404);
    $title = "Page Not Found";
    $description = "Page not found";
    $page = "404";
}

$css_path = "/css/" . $page . ".css";
$html_path = $page . ".html";
$js_path = "/js/" . $page . ".js";

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php echo $title; ?></title>
    <meta name="description" content="<?php echo $description; ?>">
    <link rel="stylesheet" href="/css/base.css">
    <?php if (file_exists("." . $css_path)) {?>
    <link rel="stylesheet" href="<?php echo $css_path; ?>">
    <?php }?>
</head>
<body>
    <header>
        <a id="burger" href="#"><img src="/img/burger.svg" alt="Burger Menu"></a>
        <img id="mobile-logo" src="/img/logo.svg" alt="Te Awamutu Bowling Club Logo">
        <nav>
            <img src="/img/logo.svg" alt="Te Awamutu Bowling Club Logo">
            <ul>
                <li><a href="/">Home</a></li>
                <li><a href="/theclub">The Club</a></li>
                <li><a href="/contact">Contact</a></li>
                <li><a href="/events">Events</a></li>
                <li><a href="/programme">Programme</a></li>
                <li><a href="/results">Results</a></li>
            </ul>
        </nav>
    </header>
    <main>
<?php
include $html_path;
?>
    </main>
    <footer>
        <p>Created by <a target="_blank" href="https://thekiwiclick.tk/">The Kiwi Click</a></p>
    </footer>
    <?php if (file_exists("." . $js_path)) {?>
        <script src="<?php echo $js_path; ?>"></script>
    <?php }?>
    <script src="/js/base.js"></script>
</body>

</html>