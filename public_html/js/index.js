let slideshowImage = document.querySelector("#slideshow img");

let numPhotos = 4;
let i = 1;
setInterval(function () {
    if (i < numPhotos) {
        i++;
    } else {
        i = 1;
    }
    slideshowImage.src = "/img/home/slideshow/" + i + ".jpg";
}, 3000);