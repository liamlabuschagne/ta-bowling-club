document.querySelector("#burger").addEventListener("click", () => {
    if (document.querySelector("nav").className == "nav-show") {
        document.querySelector("nav").className = "";
        document.querySelector("main").style.display = "block";
    } else {
        document.querySelector("nav").className = "nav-show";
        document.querySelector("main").style.display = "none";
    }
});